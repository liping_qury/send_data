# coding=utf-8
"""
train query 分类（先过词典，再过模型）
"""
import sys
from mysqlSearch import mymysql
from pb_bert import pb_predict

label_first = ['news', 'shopping', 'life', 'else']
label_second = ["sports_news", "art_and_entertainment_news", "social_news", "technology_news", "business_news",
                "political_news", "else_news", "daily_necessities", "fashion", "electronic_product", "leisure_products",
                "car", "else_shopping", "healthcare_services", "jobs", "education", "activity", "menu", "trip",
                "else_life"]


def load_sub(table):
    m = mymysql()
    dic = {}
    if table == "knowledge_query":
        search_command = 'select query, popularity from %s' % table
        query_list = list(m.search(search_command))
        for item in query_list:
            popular = item[1]
            if not popular: popular = 0
            if len(item[0]) < 2: continue  # 单子符过滤
            dic[item[0].strip().lower()] = ['', popular]
    else:
        search_command = 'select query, tag, popularity from %s' % table
        query_list = list(m.search(search_command))
        for item in query_list:
            popular = item[2]
            if not popular: popular = 0
            if len(item[0]) < 2: continue  # 单子符过滤
            dic[item[0].strip().lower()] = [item[1], popular]
    return dic


def load_topic_model():
    print('load news dict ……')
    news = load_sub('news_query_tag')
    print('load shopping dict ……')
    shopping = load_sub('shopping_query_tag')
    print('load life dict ……')
    life = load_sub('life_query_tag')
    print('load entertainment dict ……')
    entertainment = load_sub('entertainment_query_tag')
    print('load knowledge dict ……')
    knowledge = load_sub('knowledge_query')
    topic_dic = {"news": news, "shopping": shopping, "life": life, "entertainment": entertainment,
                 "knowledge": knowledge}
    print('load topic dict over')

    vocab_file = "../model/vocab.txt"
    pb_model_first = "../model/first/saved_model/1"
    pb_model_second = "../model/second/saved_model/1"
    model_first = pb_predict(label_first, pb_model_first, vocab_file)
    model_second = pb_predict(label_second, pb_model_second, vocab_file)

    print('load topic model over')
    return topic_dic, model_first, model_second

class QueryClass:
    def __init__(self):
        self.topic_dic, self.model_first, self.model_second = load_topic_model() #, self.model_second

    def load_topic_cls(self, query):


        topic = {"news": ["sports_news", "art_and_entertainment_news", "social_news", "technology_news", "business_news",
                          "political_news", "else_news"],
                 "shopping": ["daily_necessities", "fashion", "electronic_product", "leisure_products", "car",
                              "else_shopping"],
                 "life": ["healthcare_services", "jobs", "education", "activity", "menu", "trip", "else_life"]}
        second_cls = ["sports_news", "art_and_entertainment_news", "social_news", "technology_news", "business_news",
                      "political_news", "else_news", "daily_necessities", "fashion", "electronic_product",
                      "leisure_products",
                      "car", "else_shopping", "healthcare_services", "jobs", "education", "activity", "menu", "trip",
                      "else_life"]
        second_ind = [0, 7, 13, 20]
        topic_index = {}
        for k, v in topic.items():
            for s in v:
                topic_index[s] = k
        # 分类判断
        fir = list(self.model_first.predict(query))
        sec = list(self.model_second.predict(query))

        max1 = max(fir)
        max2 = max(sec)
        if max2 > 0.9:
            second = second_cls[sec.index(max2)]
            first = topic_index[second]
            return first, second
        elif max1 > 0.6:
            ind = fir.index(max1)
            first = label_first[ind]
            if first == 'else':
                return first, ''
            tmp_topic = second_cls[second_ind[ind]:second_ind[ind + 1]]
            tmp_score = sec[second_ind[ind]:second_ind[ind + 1]]
            tmp_max = max(tmp_score)
            if tmp_max > 0.3:
                second = tmp_topic[tmp_score.index(tmp_max)]
                return first, second
            if max2 < 0.4:
                return first, 'else_' + first
        elif max2 > 0.6:
            second = second_cls[sec.index(max2)]
            first = topic_index[second]
            return first, second
        else:
            return 'else', ''

        return 'else', ''

    def fun(self, query):
        first_class = 'else'
        second_class = ''
        popular = -1
        # 词典分类结果
        bol_dic = 0
        for topic, dic in self.topic_dic.items():
            if (query in dic) and dic[query][1] > popular:
                first_class = topic
                second_class = dic[query][0]
                popular = dic[query][1]
                bol_dic = 1
        # 模型分类结果
        if bol_dic == 0:
            first_class, second_class = self.load_topic_cls(query)

        return first_class, second_class


if __name__ == "__main__":

    qc = QueryClass()

    print('please input query test model …… ')
    for line in sys.stdin:
        query = line.strip().lower()
        cls = qc.fun(query)  # 计算分类结果
        print(cls)


