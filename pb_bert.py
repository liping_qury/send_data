"""
19.6.24
good version for saved model
predict based terminal
"""

import tensorflow as tf
import numpy as np
import json
import codecs
import pickle
import os
import sys
sys.path.append("/Users/edz/Desktop/query_code/rank/utils")
import tokenization, modeling
from args_parser import get_args_parser
from tensorflow.contrib import predictor

args = get_args_parser()
batch_size = 1


class pb_predict:
    def __init__(self, label_list, pb_model_dir, vocab_file):
        self.label_list = label_list
        self.pb_model_dir = pb_model_dir
        self.vocab_file = vocab_file
        self.tokenizer = tokenization.FullTokenizer(
            vocab_file=self.vocab_file,
            do_lower_case=args.do_lower_case)

        self.predict_fn = predictor.from_saved_model(pb_model_dir)


    def convert_id_to_label(self, probabilities, idx2label):
        """
        将id形式的结果转化为真实序列结果
        :param probabilities:
        :param idx2label:
        :return:
        """
        result = []
        for row in range(batch_size):
            curr_seq = []
            for ids in probabilities[row][0]:
                if ids == 0:
                    break
                curr_label = idx2label[ids]
                curr_seq.append(curr_label)
            result.append(curr_seq)
        return result

    def convert_single_example(self, example, max_seq_length, tokenizer):
        """
        Converts a single `InputExample` into a single `InputFeatures`.
        :param ex_index: index
        :param example: 一个样本
        :param label_list: 标签列表
        :param max_seq_length:
        :param tokenizer:
        """
        tokens = example
        # 序列截断
        if len(tokens) >= max_seq_length - 1:
            tokens = tokens[0:(max_seq_length - 2)]  # -2 的原因是因为序列需要加一个句首和句尾标志
        ntokens = []
        segment_ids = []

        ntokens.append("[CLS]")  # 句子开始设置CLS 标志
        segment_ids.append(0)
        for i, token in enumerate(tokens):
            ntokens.append(token)
            segment_ids.append(0)
            # label_ids.append(0)
        ntokens.append("[SEP]")  # 句尾添加[SEP] 标志
        segment_ids.append(0)
        # label_ids.append(label_map["[SEP]"])
        input_ids = tokenizer.convert_tokens_to_ids(ntokens)  # 将序列中的字(ntokens)转化为ID形式
        input_mask = [1] * len(input_ids)

        # padding, 使用
        while len(input_ids) < max_seq_length:
            input_ids.append(0)
            input_mask.append(0)
            segment_ids.append(0)

        assert len(input_ids) == max_seq_length
        assert len(input_mask) == max_seq_length
        assert len(segment_ids) == max_seq_length

        # 结构化为一个类
        feature = {
            'input_ids': np.reshape([input_ids], (batch_size, args.max_seq_length)),
            'input_mask': np.reshape([input_mask], (batch_size, args.max_seq_length)),
            #'segment_ids': self.create_int_feature(segment_ids),
        }
        return feature

    def predict(self, query):
        query = self.tokenizer.tokenize(query)
        """
        input_ids, input_mask, segment_ids = self.convert(query)
        feed_dict = {self.input_ids_p: input_ids,
                     self.input_mask_p: input_mask}
        probabilities_result = self.sess.run([self.probabilities], feed_dict)
        #pred_label_result = convert_id_to_label(probabilities_result, id2label)
        return probabilities_result[0][0].tolist()
        """
        feature = self.convert_single_example(query, args.max_seq_length, self.tokenizer)
        res = self.predict_fn(feature)['probabilities'][0]
        return res


if __name__ == "__main__":
    label_list = ['news', 'shopping', 'life', 'else']
    pb_model_dir = "../model/first/saved_model/1"
    vocab_file = "../model/vocab.txt"

    model = pb_predict(label_list, pb_model_dir, vocab_file)

    query = "dress women"
    probabilities_result = model.predict(query)
    print(probabilities_result)

    #todo: 组合策略
    # result = strage_combined_link_org_loc(query, pred_label_result[0])

    """
    
    with tf.Session(graph=tf.Graph()) as sess:
        tf.saved_model.loader.load(sess, ["serve"], pb_model_dir_small)
        graph = tf.get_default_graph()
    
        input_ids_p = tf.get_default_graph().get_tensor_by_name("input_ids:0")
        input_mask_p = tf.get_default_graph().get_tensor_by_name("input_mask:0")
        probabilities = tf.get_default_graph().get_tensor_by_name("pred_prob:0")
    
    
        dic = {}
        query = "dress woman"
        query = tokenizer.tokenize(query)
        input_ids, input_mask, segment_ids = convert(query)
        dic['input_ids'] = input_ids[0].tolist()
        dic['input_mask'] = input_mask[0].tolist()
        res = {'instances': [dic]}
        print(json.dumps(res))
    """






